package main

import (
	"fmt"
	"regexp"
	"strings"
	"time"

	log "github.com/Sirupsen/logrus"
	gitlabv1 "gitlab.com/gitlab-org/distribution/gitlab-operator/pkg/apis/gitlaboperator/v1"
	batchv1 "k8s.io/api/batch/v1"
	"k8s.io/api/core/v1"
	meta_v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

// Handler interface contains the methods that are required
type Handler interface {
	Init() error
	ObjectCreated(obj interface{})
	ObjectDeleted(obj interface{})
	ObjectUpdated(objNew, event interface{})
}

// UpdatesHandler is a sample implementation of Handler
type UpdatesHandler struct {
	client kubernetes.Interface
}

// Init handles any handler initialization
func (t *UpdatesHandler) Init() error {
	log.Info("UpdatesHandler.Init")
	return nil
}

// ObjectCreated is called when an object is created
func (t *UpdatesHandler) ObjectCreated(obj interface{}) {
	log.Info("UpdatesHandler.ObjectCreated")
	gitlab := obj.(*gitlabv1.Gitlab)
	log.Infof("    ResourceVersion: %s", gitlab.ObjectMeta.ResourceVersion)

	// This does absolutely nothing, just a place holder for the future
}

// ObjectDeleted is called when an object is deleted
func (t *UpdatesHandler) ObjectDeleted(obj interface{}) {
	log.Info("UpdatesHandler.ObjectDeleted")
	// This too does absolutely nothing
}

// ObjectUpdated is called when an object is updated
func (t *UpdatesHandler) ObjectUpdated(objNew, event interface{}) {
	rawEvent := event.(Event)
	log.Info("UpdatesHandler.ObjectUpdated")

	log.Infof("Recieved %s ", rawEvent.eventType)
	switch rawEvent.eventType {
	case "update_image":
		t.updateGitlab(objNew)
	}
}

func (t *UpdatesHandler) updateGitlab(objNew interface{}) error {
	log.Info("Gitlab update initiated")

	gitlab := objNew.(*gitlabv1.Gitlab)

	err := t.rollingUpdateGitlab(gitlab)
	if err != nil {
		return err
	}
	log.Printf("Running migrations")
	err = t.runPostMigrations(gitlab)
	if err != nil {
		return err
	}
	err = t.rollingUpdateGitlab(gitlab)
	if err != nil {
		return err
	}

	return nil
}

func (t *UpdatesHandler) rollingUpdateGitlab(obj interface{}) error {
	gitlab := obj.(*gitlabv1.Gitlab)

	namespace := gitlab.ObjectMeta.Namespace
	options := meta_v1.ListOptions{LabelSelector: "app in (unicorn, sidekiq)"}
	deploymentsInterface := t.client.AppsV1beta1().Deployments(namespace)
	deployments, err := deploymentsInterface.List(options)

	if err != nil {
		return err
	}
	re := regexp.MustCompile(`-(ce|ee)`)

	for _, deployment := range deployments.Items {
		for i, _ := range deployment.Spec.Template.Spec.Containers {
			currentImage := deployment.Spec.Template.Spec.Containers[i].Image

			im := strings.Split(currentImage, ":")[0]
			im = fmt.Sprintf("%s:%s", re.ReplaceAllString(im, fmt.Sprintf("-%s", gitlab.Spec.Type)), gitlab.Spec.Version)

			deployment.Spec.Template.Spec.Containers[i].Image = im
		}
		// we also need to inject an annotation so that we can trigger a rolling restar
		// if the image did not change
		deployment.Spec.Template.ObjectMeta.Annotations["lastRestart"] = time.Now().Format("20060102150405")

		_, err := deploymentsInterface.Update(&deployment)
		//	log.Printf("Updating now ! %v ", deployment)
		if err != nil {
			log.Printf("Error is %v ", err)
			return err
		}
	}
	return nil
}

func (t *UpdatesHandler) runPostMigrations(obj interface{}) error {
	// get migrations job

	gitlab := obj.(*gitlabv1.Gitlab)
	batchClient := t.client.BatchV1()

	jobsClient := batchClient.Jobs(gitlab.ObjectMeta.Namespace)
	jobs, err := jobsClient.List(meta_v1.ListOptions{LabelSelector: "app=migrations"})

	if err != nil {
		log.Fatalf("erros is %v ", err)
		return err
	}
	currentMigrationsJob := jobs.Items[0]

	// generate a migration name
	now := time.Now()

	newJobName := fmt.Sprintf("%s.%s", currentMigrationsJob.Name, now.Format("20060102150405"))
	// Create the job
	newJob := &batchv1.Job{
		TypeMeta: meta_v1.TypeMeta{
			Kind:       "Job",
			APIVersion: "v1",
		},
		ObjectMeta: meta_v1.ObjectMeta{
			Name: newJobName,
			Labels: map[string]string{
				"operator": "gitlab-updater",
			},
		},
		Spec: batchv1.JobSpec{
			Template: v1.PodTemplateSpec{
				ObjectMeta: meta_v1.ObjectMeta{
					Name: newJobName,
					Labels: map[string]string{
						"operator": "gitlab-updater",
					},
				},
				Spec: currentMigrationsJob.Spec.Template.Spec,
			},
		},
	}

	re := regexp.MustCompile(`-(ce|ee)`)
	for i, _ := range newJob.Spec.Template.Spec.Containers {
		currentImage := newJob.Spec.Template.Spec.Containers[i].Image

		im := strings.Split(currentImage, ":")[0]
		im = fmt.Sprintf("%s:%s", re.ReplaceAllString(im, fmt.Sprintf("-%s", gitlab.Spec.Type)), gitlab.Spec.Version)

		newJob.Spec.Template.Spec.Containers[i].Image = im
	}

	_, err = jobsClient.Create(newJob)

	if err != nil {
		log.Fatalf("error is %v ", err)
	}

	// Now we should wait for the job to finish excution!
	// This channel will block until the goroutine frees it
	stop := make(chan int)

	go func() {
		for {
			job, err := jobsClient.Get(newJobName, meta_v1.GetOptions{})

			if err != nil {
				log.Fatal(err)
			}

			if job.Status.Succeeded == 1 {
				break
			}
			time.Sleep(5 * time.Second)
		}
		close(stop)
	}()

	<-stop
	log.Info("Migrations completed")
	return nil
}
