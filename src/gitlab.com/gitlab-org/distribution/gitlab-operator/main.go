package main

import (
	"os"
	"os/signal"
	"syscall"

	log "github.com/Sirupsen/logrus"
	gitlabv1 "gitlab.com/gitlab-org/distribution/gitlab-operator/pkg/apis/gitlaboperator/v1"

	gitlabclientset "gitlab.com/gitlab-org/distribution/gitlab-operator/pkg/client/clientset/versioned"
	gitlabinformer_v1 "gitlab.com/gitlab-org/distribution/gitlab-operator/pkg/client/informers/externalversions/gitlaboperator/v1"
	meta_v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/workqueue"

	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
)

// retrieve the Kubernetes cluster client from outside of the cluster
func getKubernetesClient() (kubernetes.Interface, *gitlabclientset.Clientset) {
	// construct the path to resolve to `~/.kube/config`
	kubeConfigPath := os.Getenv("HOME") + "/.kube/config"

	// create the config from the path
	config, err := clientcmd.BuildConfigFromFlags("", kubeConfigPath)
	if err != nil {
		log.Fatalf("getClusterConfig: %v", err)
	}

	// generate the client based off of the config
	client, err := kubernetes.NewForConfig(config)
	if err != nil {
		log.Fatalf("getClusterConfig: %v", err)
	}

	gitlabClient, err := gitlabclientset.NewForConfig(config)

	if err != nil {
		log.Fatalf("getClusterConfig: %v", err)
	}

	log.Info("Successfully constructed k8s client")
	return client, gitlabClient
}

func main() {
	client, gitlabClient := getKubernetesClient()

	informer := gitlabinformer_v1.NewGitlabInformer(
		gitlabClient,
		meta_v1.NamespaceAll,
		0,
		cache.Indexers{},
	)

	queue := workqueue.NewRateLimitingQueue(workqueue.DefaultControllerRateLimiter())
	event := Event{resourceType: "gitlab"}

	informer.AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: func(obj interface{}) {
			key, err := cache.MetaNamespaceKeyFunc(obj)

			event.key = key
			event.eventType = "create"

			log.Infof("Add Gitlab resource: %s", key)
			if err == nil {
				queue.Add(event)
			}
		},
		UpdateFunc: func(oldObj, newObj interface{}) {
			key, err := cache.MetaNamespaceKeyFunc(newObj)
			log.Infof("Update Gitlab resource  %s", key)

			oldD := oldObj.(*gitlabv1.Gitlab)
			newD := newObj.(*gitlabv1.Gitlab)

			if oldD.Spec.Version != newD.Spec.Version || oldD.Spec.Type != newD.Spec.Type {
				event.eventType = "update_image"
			} else {
				event.eventType = "update" // This is in case of changing labels or annotations
			}
			event.key = key

			if err == nil {
				queue.Add(event)
			}
		},
		DeleteFunc: func(obj interface{}) {
			key, err := cache.DeletionHandlingMetaNamespaceKeyFunc(obj)
			event.key = key
			event.eventType = "delete"
			log.Infof("Delete Gitlab: %s", key)
			if err == nil {
				queue.Add(event)
			}
		},
	})

	controller := Controller{
		logger:    log.NewEntry(log.New()),
		clientset: client,
		informer:  informer,
		queue:     queue,
		handler:   &UpdatesHandler{client: client},
	}

	stopCh := make(chan struct{})
	defer close(stopCh)

	go controller.Run(stopCh)

	sigTerm := make(chan os.Signal, 1)
	signal.Notify(sigTerm, syscall.SIGTERM)
	signal.Notify(sigTerm, syscall.SIGINT)
	<-sigTerm
}
